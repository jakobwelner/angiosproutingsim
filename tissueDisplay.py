import config
import numpy as np
import cv2

class TissueDisplay():
    def __init__(self, tissue):
        self.tissue = tissue
        self.height, self.width = tissue.getDim()


    def labelLayer(self, img, label, pos, size=1, thickness=1):
        font = cv2.FONT_HERSHEY_PLAIN
        cv2.putText(img, label, pos, font, size, (config.DVALMAX,config.DVALMAX,config.DVALMAX),thickness)


    def display(self, actSpeed, timecode):
        zl = np.zeros((self.height, self.width), dtype=config.DATATYPE) # ZeroLayer, hacked way to make channels match when combining into layout


        ######################################################
        #### Get image channels
        ######################################################
        #b = tissue.getCo2Img() # Disabling CO2 layer
        b = zl
        g = self.tissue.getVegfaImg()
        r = self.tissue.getO2Img()



        ### showing cooldown - temp solution
        #vessels = self.tissue.get_vessels()
        #gcopy = g.copy()
        #vessels.draw_vegfa_cooldown(gcopy)



        ######################################################
        #### Merging all channels
        ######################################################
        self.combImg = cv2.merge((b, g, r))
        #self.combImg = cv2.merge((b, gcopy, r))
        vs = self.tissue.getVesselStructure()
        self.combImg = cv2.add(self.combImg, cv2.merge((vs,vs,vs))) # Superimposing vessel structure in white


        ######################################################
        #### Merge to 3-channel images for combining into layout
        ######################################################
        rc = cv2.merge((zl,zl,r))
        gc = cv2.merge((zl,g,zl))
        bc = cv2.merge((b, zl,zl)) # CO2 Layer


        ######################################################
        #### Adding labels to images
        ######################################################
        #labelLayer(bc, "CO2", (width-100, height-10))
        self.labelLayer(gc, "VEGF-A", (self.width-100, self.height-10))
        self.labelLayer(rc, "O2", (self.width-100, self.height-10))
        self.labelLayer(self.combImg, "Combined", (self.width-100, self.height-10))
        self.labelLayer(self.combImg, actSpeed, (10, self.height-10))
        self.labelLayer(self.combImg, timecode, (20, 30), 2, 2)


        ######################################################
        #### Compiling layout image
        ######################################################
        #col1 = np.concatenate((combImg, rc, gc, bc), axis=1)
        #col1 = np.concatenate((combImg, rc, gc), axis=1)


        ######################################################
        #### Showing images on screen
        ######################################################
        #small = cv2.resize(col1, (0,0), fx=0.5, fy=0.5)
        #cv2.imshow("Layout", small)
        #cv2.imshow("Layout", col1)

        #cv2.imshow("Growth Marked Layer", tissue.growth_marked_layer)
        cv2.imshow("Combined Channels", self.combImg)
        #cv2.imshow("CO2", b)
        #cv2.imshow("VEGF-A", g)
        #cv2.imshow("O2", r)

    def get_comb_img(self):
        return self.combImg

import cv2

def displayOcvImg(img):
    cv2.startWindowThread()
    if type(img) == type(list()):
        count = 1
        for i in img:
            cv2.imshow("preview" + str(count), i)
            count +=1
    else:
        cv2.imshow("preview", img)
    cv2.waitKey()
    cv2.destroyAllWindows()

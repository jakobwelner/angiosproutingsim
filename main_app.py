import tissue
import config
import tissueDisplay
import cv2
import numpy as np
import time

#import timing

reload(tissue)
reload(config)
reload(tissueDisplay)


def run():
    if not config.VIEWER and not config.SIMLENGTH > 0:
        raise ValueError("Need to set SIMLENGTH when running without viewer")

    if config.VIEWER:
        cv2.startWindowThread()

    tissueObj = tissue.Tissue(config.HEIGHT, config.WIDTH, config.STEPSIZE)
    simstart = tstart = time.time()

    wait = 1
    maxVal = 0
    actualPassedTime = 0.0
    stepCounter = 0 # Evaluate speed every SHOWSTEPS steps

    display = tissueDisplay.TissueDisplay(tissueObj)

    try:
        while True:
            ######################################################
            #### Evaluating next timestep
            ######################################################
            #timing.log("before")
            tissueObj.update()
            #timing.log("after")

            if not config.VIEWER:
                actualPassedTime += config.STEPSIZE
                if actualPassedTime > config.SIMLENGTH:
                    break

            elif stepCounter == 0:
                ######################################################
                #### Calculate framerate and timecode
                ######################################################
                newTime = time.time()
                elapse = (newTime - tstart)/float(config.SHOWSTEPS)
                actSpeed = "Actual speed: " + str( np.round_((config.STEPSIZE)/elapse, decimals=3))
                tstart = newTime

                actualPassedTime += config.STEPSIZE * config.SHOWSTEPS
                m, s = divmod(actualPassedTime, 60)    # Counting simulated time
                h, m = divmod(m, 60)
                timecode = "%d:%02d:%02d" % (h, m, s)

                wait = max(1, (wait + int(((config.STEPSIZE/config.SPEEDMULT) - elapse)*1000) ))

                ######################################################
                #### Show layers with actual speed and timecode
                ######################################################
                display.display(actSpeed, timecode)

                # Resetting stepCounter
                stepCounter = config.SHOWSTEPS
            else:
                # skipping viewer until SHOWSTEPS runs
                stepCounter -= 1


            keystroke = cv2.waitKey(wait)
            #keystroke = ord('p') # Temp if wanting to see first frame
            if keystroke == ord('q'):
                print "Quitting by user input"
                break
            elif keystroke == ord('s'):
                #cv2.imwrite("images/output/o2Layer.jpg", rc)
                #cv2.imwrite("images/output/vegfaLayer.jpg", tissueObj.getVegfaImg())
                #cv2.imwrite("images/output/co2Layer.jpg", bc)
                cv2.imwrite("images/output/combImg.jpg", display.get_comb_img())
            elif keystroke == ord('p'):
                ks = None
                while not ks == ord('p'):
                    ks = cv2.waitKey(0)
                    if ks == ord('q'):
                        raise KeyboardInterrupt



    except KeyboardInterrupt:
        print "Released Video Resource"

        if config.VIEWER:
            cv2.destroyAllWindows()

    finally:
        newTime = time.time()
        actSimSpeed = np.round_(actualPassedTime/(newTime - simstart), decimals=0)

        m, s = divmod( (newTime - simstart), 60)    # Counting simulated time
        h, m = divmod(m, 60)
        timecode = "%d:%02d:%02d" % (h, m, s)

        m, s = divmod( actualPassedTime, 60)    # Counting simulated time
        h, m = divmod(m, 60)
        simtime = "%d:%02d:%02d" % (h, m, s)

        print "\n#### Stats Output ####"
        print "Elapsed time: ", timecode
        print "Average simulation speed: ", actSimSpeed, "x realtime"
        print "Simulated time: ", simtime
        print "StepSize: ", config.STEPSIZE
        print "TissueSize: ", config.HEIGHT, "x", config.WIDTH

        if config.VIEWER:
            cv2.destroyAllWindows()

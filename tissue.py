import config
import vessels
import numpy as np
import cv2

reload(config)
reload(vessels)

class Tissue():

    def __init__(self, height, width, stepsize):

        # Define variables
        self.height = height
        self.width = width
        self.stepsize = stepsize

        # Normalizing rate values
        config.METABOLIC_RATE *= self.stepsize * config.DVALMAX
        config.VEGFA_PRODUCTION_RATE *= self.stepsize * config.DVALMAX
        config.VEGFA_DEGRADE_RATE *= self.stepsize * config.DVALMAX

        # Define layers
        self.o2Layer = np.zeros((height, width), dtype=config.DATATYPE)
        self.co2Layer = np.zeros((height, width), dtype=config.DATATYPE)
        self.vegfaLayer = np.zeros((height, width), dtype=config.DATATYPE)

        # Initiate instance of Vessel class
        self.vessels = vessels.Vessels(height, width)
        initial_vessel_structure = config.init_vessel_structure
        self.vessels.import_structure(initial_vessel_structure)
        self.vessels.update_vessels_img()


    def update(self):
        # O2 supply from arteries
        self.o2Layer = cv2.add(self.o2Layer, self.vessels.getO2Supply())

        # Absorption of CO2
        self.co2Layer = cv2.subtract(self.co2Layer, self.vessels.get_layer())

        # Tissue metabolism
        self.metabolise()

        # Hypoxic state
        # Where there is no O2, add vegfaRate to current vegfaLayer
        self.vegfaLayer = np.where(self.o2Layer < config.MIN_METABOLIC_O2_REC, cv2.add(self.vegfaLayer, config.VEGFA_PRODUCTION_RATE), self.vegfaLayer)

        # Diffusion through tissue
        self.diffuse()

        # Grow Vessels Towards VEGF-A
        #infl_points = self.vessels.get_vegfa_influence(self.vegfaLayer)
        #self.growth_marked_layer = self.vessels.draw_growth_points(self.vegfaLayer, infl_points)
        #self.vessels.draw_vegfa_cooldown(self.growth_marked_layer)

        self.vessels.update(self.vegfaLayer)


    def metabolise(self):

        # Where there is enough oxygen, perform metabolism
        self.co2Layer = np.where(self.o2Layer > config.METABOLIC_RATE,
                                 cv2.add(self.co2Layer, config.METABOLIC_RATE * config.CO2_TO_O2_RATIO),
                                 self.co2Layer)
        self.o2Layer = np.where(self.o2Layer > config.METABOLIC_RATE,
                                 cv2.subtract(self.o2Layer, config.METABOLIC_RATE),
                                 self.o2Layer)

        self.vegfaLayer = cv2.subtract(self.vegfaLayer, config.VEGFA_DEGRADE_RATE)


    def diffuse(self):
        if 0:
            self.o2Layer = cv2.GaussianBlur(self.o2Layer,
                                    (config.O2_DIFFUSION_SPEED,config.O2_DIFFUSION_SPEED), 0)
            self.vegfaLayer = cv2.GaussianBlur(self.vegfaLayer,
                                    (config.VEGFA_DIFFUSION_SPEED,config.VEGFA_DIFFUSION_SPEED), 0)
            self.co2Layer = cv2.GaussianBlur(self.co2Layer,
                                    (config.CO2_DIFFUSION_SPEED,config.CO2_DIFFUSION_SPEED), 0)

        else:
            self.o2Layer = cv2.blur(self.o2Layer,
                                    (config.O2_DIFFUSION_SPEED,config.O2_DIFFUSION_SPEED))
            self.vegfaLayer = cv2.blur(self.vegfaLayer,
                                    (config.VEGFA_DIFFUSION_SPEED,config.VEGFA_DIFFUSION_SPEED))
            self.co2Layer = cv2.blur(self.co2Layer,
                                    (config.CO2_DIFFUSION_SPEED,config.CO2_DIFFUSION_SPEED))


    def getO2Img(self):
        return self.o2Layer

    def getVegfaImg(self):
        return self.vegfaLayer

    def getCo2Img(self):
        return self.co2Layer

    def getVesselStructure(self):
        return self.vessels.get_layer()

    def getDim(self):
        return self.height, self.width

    def get_vessels(self):
        return self.vessels

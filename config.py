import numpy as np

DATATYPE = np.uint16
DVALMAX = 65535

VEGFA_INFLUENCE_LIMIT = DVALMAX/20 # Limit for VEGF-A to initiate growth

VIEWER = True
SIMLENGTH = 5 # [Seconds] To be used in conjunction with VIEWER
SHOWSTEPS = 10 # Show every # steps

SPEEDMULT = 100 # 1 for realtime
STEPSIZE = 0.1 # sec

HEIGHT = 500
WIDTH = 600


# Tissue Config:
CO2_TO_O2_RATIO       = 1.0 # Fraction of how much CO2 is made per O2 for metabolism

METABOLIC_RATE        = 0.05 # normalized rate per second
VEGFA_PRODUCTION_RATE = 0.03 # normalized rate per second
VEGFA_DEGRADE_RATE    = 0.01 # normalized rate per second

O2_DIFFUSION_SPEED    = 5   # diameter of smooth kernel
CO2_DIFFUSION_SPEED   = 21   # diameter of smooth kernel
VEGFA_DIFFUSION_SPEED = 21   # diameter of smooth kernel

MIN_METABOLIC_O2_REC  = 0.01


# Vessels Config
BLOOD_SUPPLY_SPEED = 7
VEGFA_COOLDOWN_DIAMETER = 100
VEGF_COOLDOWN_TIME = 1000
MIN_VEGFA_GRADIENT_MAG = 4000


init_vessel_structure = {"nodes": [
        (0, 56),
        (200,53),
        (400,83),
        (WIDTH,30)
    ],
              "connections":[ # pairs of "nodes" list indiced that are connected
        (0,1),
        (1,2),
        (2,3)
    ]}

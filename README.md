# Logic of Vessel Growth:
If (connection feels VEGF-A above limit)
- sprout:
  - mark for vegfa cooldown: draw black circle at sprout position when selecting connection. Set cooldown counter
  - make new filopodia-type node
  - insert new node at contact point
  - connect nodes with sprout-type connection (No O2 diffusion before mature)

## For Vessel Update:
- increase maturity for immature node connections (not sprouts). if maturity limit exceeded convert to mature node connection (With O2 diffusion)
- decrease vegfa cooldown counter by 1 and remove when 0
- for each filopodia:
  - check VEGF-A levels and gradient strength at filopodia.
  - If (still above limit):
    - apply random chance of splitting
    - if (split):
      - make two new filopodia nodes
      - convert old filopodia node to immature vessel
      - apply little offset to new filopodia
  - else:
    - convert to normal Node
    - start maturing connection

- find VEGF-A gradient vector for all filopodia
- if (gradient is above limit):
  - move filopodia-nodes slightly towards gradient width noise.
- else:
  - *maybe close to angiogenic centre.*
  - Stop moving and convert to immature vessel

# Implement in steps
insert_node
sprout_at function - insert vesselNode at sprouting origin, add filopodia-node and connect to sprout origin
filopodia /vessel node type in class
move-node function


# Todo:
- Find a way to stop sprouting when reaching destination
- Find a way to remove VEGF-A where new vessels have grown, before too many new sprouts start
- Introduce stochasticity in sprouting direction
- Introduce stochasticity in sub-node generation on sprout


# How to run:
- Install python library: cv2 and numPy
- from a python shell run: 'import main_app; main_app.run()'

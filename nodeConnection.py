import uuid

class NodeConnection():
    def __init__(self, nodeID1, nodeID2, thickness):
        self.nodeIDs = [nodeID1, nodeID2]
        self.ID = uuid.uuid4().hex
        self.thickness = thickness

    def get_ID(self):
        return self.ID

    def get_nodeIDs(self):
        return self.nodeIDs

    def get_thickness(self):
        return self.thickness

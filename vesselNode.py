import uuid

class VesselNode():
    def __init__(self, position, ntype = 1):
        """
        ntype:
          1: Mature Vessel Node
          2: Filopodia
        """
        self.ID = uuid.uuid4().hex
        self.position = position
        self.ntype = ntype

    def get_ID(self):
        return self.ID

    def get_pos(self):
        return self.position

    def get_type(self):
        return self.ntype

    def set_type(self, ntype):
        self.ntype = ntype

    def move_absolute(self, pos):
        self.position = pos

    def move_relative(self, offset):
        endPos = (self.position[0] + offset[0], self.position[1] + offset[1])
        self.move_absolute(endPos)

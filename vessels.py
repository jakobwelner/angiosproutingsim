import config
import vesselNode
import nodeConnection
import numpy as np
import cv2

reload(config)
reload(vesselNode)
reload(nodeConnection)

class Vessels():
    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.img = None
        self.nodes = {}
        self.filopods = {}
        self.connections = {}
        self.vegfa_cooldown = {} # format {cooldown, position (x,y)}
        self.clear_vessels_img()


    def make_node(self, position, ntype = 1):
        node = vesselNode.VesselNode(position, ntype)
        nodeID = node.get_ID()
        self.nodes[nodeID] = node
        return nodeID



    def insert_node(self, pos, connID):
        conn = self.connections[connID]
        endNodeIDs = conn.get_nodeIDs()
        newNodeID = self.make_node(pos)
        self.__remove_connection(connID)
        self.__make_connection(endNodeIDs[0], newNodeID)
        self.__make_connection(endNodeIDs[1], newNodeID)
        return newNodeID



    def __make_filopodia_from_node(self, nodeID):
        rootNode = self.nodes[nodeID]
        rootPos = rootNode.get_pos()
        filoPos = (rootPos[0], rootPos[1])
        filopodID = self.make_node(filoPos, 2) # Initiate as filopodia
        self.__make_connection(nodeID, filopodID)
        return filopodID


    def migrate_filopodia(self, vegfaLayer):
        filopods = []
        for nid, n in self.nodes.iteritems():
            if n.get_type() == 2:
                gradient = self.get_gradient_vector(vegfaLayer, n.get_pos())
                #print "Gradient at pos: ", n.get_pos()
                #print "Gradient: ", gradient
                if not gradient == None:
                    n.move_relative(gradient)
                else:
                    # Gradient too small. Stopping migration
                    print "No gradient: Converting filopod to node"
                    n.set_type(1)


    def get_gradient_vector(self, img, pos):
        sobelx = cv2.Sobel(img, cv2.CV_64F, 1,0,ksize=5)
        sobely = cv2.Sobel(img, cv2.CV_64F, 0,1,ksize=5)
        #phaseImg = cv2.phase(sobelx,sobely)
        # np.gradient()

        dx = sobelx[pos[1],pos[0]]
        dy = sobely[pos[1],pos[0]]
        vec = (dx, dy)
        mag = np.linalg.norm(vec)
        if mag < config.MIN_VEGFA_GRADIENT_MAG:
            print "Gradient magnitude below threshold. Stopping growth. Mag: ", mag, ", threshold: ", config.MIN_VEGFA_GRADIENT_MAG
            return None
        else:
            #print "Gradient Mag: ", mag
            vec = vec/mag
            #vec *= 10
            return tuple(vec)


    def make_sprout(self, pos, connID):
        print("Making a sprout")
        rootID = self.insert_node(pos, connID)
        filopodID = self.__make_filopodia_from_node(rootID)


    def __remove_connection(self, connID):
        if connID in self.connections:
            del self.connections[connID]
        else:
            raise InputError("Provided connection ID does not exist: " + str(connID))


    def __make_connection(self, nodeID1, nodeID2, thickness=5):
        if nodeID1 in self.nodes and nodeID2 in self.nodes:
            conn = nodeConnection.NodeConnection(nodeID1, nodeID2, thickness)
            connID = conn.get_ID()
            self.connections[connID] = conn
            return connID
        else:
            raise InputError("Those two ID's does not exist in this vessel container")


    def import_structure(self, vessel_dict):
        nodeIDs = []
        for node_position in vessel_dict["nodes"]:
             nodeIDs.append( self.make_node(node_position) )

        for nodeI1, nodeI2 in vessel_dict["connections"]:
            self.__make_connection(nodeIDs[nodeI1], nodeIDs[nodeI2])


    def update(self, vegfaLayer):
        infl_points = self.get_vegfa_influence(vegfaLayer)
        self.__do_vegfa_cooldown()

        if not infl_points == None:
            for connID, pos in infl_points.iteritems():
                self.make_sprout(pos, connID)

        self.migrate_filopodia(vegfaLayer)
        self.update_vessels_img()


    def update_vessels_img(self):
        # Updates the actual saved img of structure
        self.clear_vessels_img()
        for connID, conn in self.connections.iteritems():
            nodeIDs = conn.get_nodeIDs()
            pos1 = self.nodes[nodeIDs[0]].get_pos()
            pos2 = self.nodes[nodeIDs[1]].get_pos()

            cv2.line(self.img, self.__pos_to_int(pos1), self.__pos_to_int(pos2), config.DVALMAX, conn.get_thickness())


    def clear_vessels_img(self):
        self.img = np.zeros((self.height, self.width), dtype=config.DATATYPE)


    def display_vessels(self):
        cv2.startWindowThread()
        cv2.imshow("Vessels", self.img)
        cv2.waitKey()
        cv2.destroyAllWindows()


    def get_layer(self):
        return self.img

    def getO2Supply(self):
        tmp = self.img.copy()
        #return cv2.GaussianBlur(tmp,(5,5), 0) # Defining how blood penetrates the vessel walls
        return cv2.blur(tmp,(config.BLOOD_SUPPLY_SPEED,config.BLOOD_SUPPLY_SPEED)) # Defining how blood penetrates the vessel walls


    def get_vegfa_influence(self, vegfaLayer):
        # Find intersections between vegfa hormone and vessels structure.
        # Returns None if no points are found. Otherwise a dict by connID with coords touple
        #
        # Currently only 1 point per connection
        # Todo:
        # - support multiple points per connection.
        #   Ie: paint black circle on connection line at the found point
        #   Re-run connection to exclude found point and a certain diamter around it.
        vegfa_influence_points = None

        for connID, conn in self.connections.iteritems():
            sel = self.__get_connection_selection(connID, conn)

            try:
                maxI = vegfaLayer[sel].argmax(axis=0)
                maxVal = vegfaLayer[sel][maxI]

                if maxVal > config.VEGFA_INFLUENCE_LIMIT:
                    if vegfa_influence_points == None: # Inititalize
                        vegfa_influence_points = {}

                    x = sel[1][maxI]
                    y = sel[0][maxI]

                    vegfa_influence_points[connID] = (x,y)
                    self.__add_vegfa_cooldown((x,y))
                    #print "Found VEGF-A influence point at: " + str((x,y))
            except ValueError:
                pass # When whole connection is covered by cooldown. argmax fails when applying on empty selection

        return vegfa_influence_points


    def __add_vegfa_cooldown(self, pos):
        if not pos in self.vegfa_cooldown:
            self.vegfa_cooldown[pos] = config.VEGF_COOLDOWN_TIME
        else:
            pass # This is ok for now
            #print("VEGFA_cooldown: " + str(self.vegfa_cooldown))
            #print("cooldown already exist. It shouldn't go here. Cooldown val: " + str(self.vegfa_cooldown[pos]) )


    def __do_vegfa_cooldown(self):
        # perform on each update
        toDelete = []

        for pos in self.vegfa_cooldown:
            if self.vegfa_cooldown[pos] > 1:
                self.vegfa_cooldown[pos] -= 1
            else:
                toDelete.append(pos)

        for pos in toDelete:
            del self.vegfa_cooldown[pos]



    def draw_vegfa_cooldown(self, img):
        for pos in self.vegfa_cooldown:
            #print "Drawing cooldown circle at: " + str(pos)
            cv2.circle(img, self.__pos_to_int(pos), config.VEGFA_COOLDOWN_DIAMETER, 0, -1)



    def __pos_to_int(self, pos):
        return (int(pos[0]), int(pos[1]))


    def __get_connection_selection(self, connID, conn):
        # Maybe add extra argument with already-found position, which can then be excluded
        # from selection. Maybe by adding black circle at pre-found positions

        conn_img = np.zeros((self.height, self.width), dtype=config.DATATYPE)
        nodeIDs = conn.get_nodeIDs()
        pos1 = self.nodes[nodeIDs[0]].get_pos()
        pos2 = self.nodes[nodeIDs[1]].get_pos()

        #print "pos1: ", pos1, ", pos2: ", pos2
        cv2.line(conn_img, self.__pos_to_int(pos1), self.__pos_to_int(pos2), config.DVALMAX, conn.get_thickness())
        self.draw_vegfa_cooldown(conn_img)

        line_selection = np.where(conn_img == config.DVALMAX)
        return line_selection






    """
    def draw_growth_points(self, vegfaLayer, influence_points):
        newLayer = vegfaLayer.copy()

        if influence_points != None:
            for p in influence_points:
                x, y = influence_points[p]
                cv2.circle(newLayer, (x,y), 10, config.DVALMAX)
                #self.__draw_vegfa_cooldown(newLayer)
        return newLayer
    """
